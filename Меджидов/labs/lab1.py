from math import log10
from functools import reduce
from random import randint


# 1. Выведите на экран результат вычисления 7 + 4 - 344*21
def summing_and_multiplying():
    """Выводит сумму 7 + 4 - 344*21"""
    return 7 + 4 - 344*21


# 2. Выведите на экран результат вычисления  2**2 + 3**(1/2)
def exponentiation():
    """Выводит сумму 7 + 4 - 344*21"""
    return 2**2 + 3**(1/2)


# 3. Вычислите |x|+x, где x вводится с клавиатуры.
def abs_x_plus_x():
    """Получает x и суммирует со своим модулем"""
    x = int(input())
    return abs(x) + x


# 4. Вычислить гипотенузу прямоугольного треугольника. Катеты вводит пользователь.
def calc_hypotenuse():
    """Считает гипотенузу по известным катетам через теорему Пифагора"""
    a, b = int(input()), int(input())
    return (a**2 + b**2)**(1/2)


# 5. Написать программу, действующую по аналогии с игральной костью:
# случайным образом при запуске выдающую число от 1 до 6.
def get_random():
    """Генерирует случайное число от 1 до 6"""
    return int(randint(1, 6))


# 6. Найти сумму цифр трехзначного числа.
def sum_digit_of_number(num):
    """Находит сумму цифр числа"""
    sum_ = 0
    while num:
        sum_ += num % 10
        num //= 10

    return sum_


# 7. Найти сумму ряда чисел от 1 до 100.
def sum_one_to_hundred():
    """Находит сумму ряда чисел от 1 до 100"""
    return sum([i for i in range(1, 101)])


# 8. Дана последовательность чисел от 1 до 100.
# Найдите сумму остатков от деления всех элементов последовательности
# на соответственные порядковые номера.
def sum_of_mod(lst):
    """
    Возвращает сумму остатков от деления всех элементов последовательности
    на соответственные порядковые номера.
    Порядковый номер ведётся с единицы (на ноль делить ведь нельзя)
    """
    return sum([elem % i for i, elem in enumerate(lst, 1)])


# 9. Поменяйте местами первый и последний элементы списка.
def replace_first_and_last(lst):
    """Возвращает модифицированный список"""
    lst[0], lst[-1] = lst[-1], lst[0]
    return lst


# 10. Добавить элемент в начало списка.
def insert(lst, elem):
    """Возвращает список со вставленным в начало элементом"""
    lst.insert(0, 5)
    return lst


# 11. Имеется список целых чисел.
# Составьте другой список, который будет содержать
# в себе сначала все нечетные элементы списка,
# а затем все четные.
def sort_by_odd_and_even(lst):
    """
    Сортирует список таким образом,
    чтобы нечётные оказались в начале,
    а чётные в конце
    """
    return sorted(lst, key=lambda x: x % 2 == 0)


# 12. У вас есть массив чисел.
# Напишите три функции, которые вычисляют сумму этих чисел:
# с for-циклом,
# с while-циклом,
# с рекурсией, sum, reduce

def for_sum(arr):
    """Суммирует элементы списка циклом for """
    sum_ = 0
    for elem in arr:
        sum_ += elem
    return sum_


def while_sum(arr):
    """Суммирует элементы списка циклом while"""
    sum_ = 0
    i = 0
    while i < len(arr):
        sum_ += arr[i]
        i += 1
    return sum_


def recursion_sum(arr, i=0):
    """Суммирует элементы списка рекурсивно"""
    if i >= len(arr):
        return 0
    return recursion_sum(arr, i+1) + arr[i]


def sum_sum(arr):
    """Суммирует элементы списка функцией sum"""
    return sum(arr)


def reduce_sum(arr):
    """Суммирует элементы списка функцией reduce"""
    return reduce(lambda x, y: x+y, arr, 0)


# 13. Напишите функцию, которая создаёт комбинацию двух списков таким образом:
# [1, 2, 3] (+) [11, 22, 33] -> [1, 11, 2, 22, 3, 33]

def my_map(lst1, lst2):
    """Мне пока лень её делать"""
    lst = []
    if len(lst1) != len(lst2):
        return lst

    for elem1, elem2 in zip(lst1, lst2):
        lst.append(elem1)
        lst.append(elem2)

    return lst


def fibonacci(n):
    num1 = num2 = 1
    n -= 2
    while n > 0:
        num1, num2 = num2, num1 + num2
        n -= 1

    return num2


# 14. У вас есть массив чисел, составьте из них максимальное число.
# Например: [61, 228, 9] -> 961228

def first_digit(num):
    """Возвращает первую цифру числа"""
    return num//pow(10, int(log10(num)))


def list_to_number(lst):
    """Из массива чисел составляет максимальное число"""
    super_num = 0
    bit_depth = 0

    sorted_lst = sorted(lst, reverse=True)

    for num in sorted(sorted_lst, key=first_digit):
        super_num += num*(10**bit_depth)
        bit_depth += int(log10(num))+1

    return super_num

# Когда-нибудь я это улучшу и избавлюсь от eval
# Но мозгов пока не хватает
def funcs(sign):
    return {
        '+': lambda x, y: x + y,
        '-': lambda x, y: x - y,
        '_': lambda x, y: x*10 + y
    }[sign]


def concat(a, b):
    return a*10 + b


def sum_(signs):
    return 0


def rec(elem, signs, arr=('1',)):
    for func in signs:
        if elem == 10:
            if eval(''.join(arr)) == 100:
                print(''.join(arr))
            return 0

        rec(elem + 1, signs, (*arr, func, str(elem)))
