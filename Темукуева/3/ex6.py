#coding=utf-8

def f_sk():
    index = 0
    f = open('text.txt', 'r')
    s = f.read()
    f.close()

    f = open('text.txt', 'a')
    f.write('\n --New data--\n')
    if s.count('(') == s.count(')') and s.count('[') == s.count(']') and s.count('{') == s.count('}'):  # проверяю, все ли открытые скобки закрыты
        print('Yes')
    elif s.count('(') + s.count('{') + s.count('[') != s.count(')') + s.count('}') + s.count(']'): # проверяю, несовпадение открытых и закрытых скобок
        print(-1)
    else:
        for index, el in enumerate(s):      # ищу первую ошибочную скобку
            if el == '(' and s.find(')') != -1:     # если есть ( и есть )
                s = s.replace(')','',1)   # удаляю )
            elif el == '(' and s.find(')') == -1: # если есть ( и нет )
                f.write(str(index))            #вывожу индекс первой ошибочной скобки
            elif el == '{' and s.find('}') != -1:    # если есть { и есть },
                s = s.replace('}', '',1)    #удаляю }
            elif el == '{' and s.find('}') == -1:   # если есть { и нет }
                f.write(str(index))            #вывожу индекс первой ошибочной скобки
            elif el == '[' and s.find(']') != -1:   # если есть [ и есть ],
                s = s.replace(']', '',1)        #удаляю ]
            elif el == '[' and s.find(']') == -1:   # если есть [ и нет ]
                f.write(str(index))    # #вывожу индекс первой ошибочной скобки
    f.close()
f_sk ()












